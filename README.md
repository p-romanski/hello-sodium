# Hello-Sodium (libsodium/NaCl libs)

## How to build

```
cmake .
make
```

## How to generate keys
```
./genkeys alice
```
