#include <sodium.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MESSAGE (const unsigned char *)"test"
#define MESSAGE_LEN 4
#define CIPHERTEXT_LEN (crypto_box_MACBYTES + MESSAGE_LEN)

int print_bytes(char *label, const unsigned char *data, int length)
{
    int encoded_max_length = sodium_base64_ENCODED_LEN(length, sodium_base64_VARIANT_URLSAFE_NO_PADDING);
    fprintf(stderr, "DEBUG: print_bytes[%s]: data length=[%d], encoded_max_length=[%d]\n", label, length, encoded_max_length);
    char encoded_text[encoded_max_length];
    sodium_bin2base64(encoded_text, encoded_max_length, data, crypto_box_NONCEBYTES, sodium_base64_VARIANT_URLSAFE_NO_PADDING);

    fprintf(stdout, "%s: %s\n", label, data);
    fprintf(stdout, "%s: %s\n", label, encoded_text);
}

int main(void)
{
    if (sodium_init() < 0)
    {
        return -1;
    }

    unsigned char private_key[crypto_box_SECRETKEYBYTES];
    unsigned char public_key[crypto_box_PUBLICKEYBYTES];
    unsigned char nonce[crypto_box_NONCEBYTES];
    unsigned char ciphertext[CIPHERTEXT_LEN];

    // randombytes_buf(nonce, sizeof nonce);
    // print_bytes(" :Nonce          ", nonce, crypto_box_NONCEBYTES);
    // print_bytes(" :Cipher Text    ", ciphertext, CIPHERTEXT_LEN);

    unsigned char decrypted[MESSAGE_LEN + 1];
    if (crypto_box_open_easy(decrypted, ciphertext, CIPHERTEXT_LEN, nonce,
                             public_key, private_key) != 0)
    {
        return -3;
    }

    decrypted[MESSAGE_LEN] = '\0';
    fprintf(stdout, " :Decrypted Msg  : %s\n", decrypted);

    return 0;
}
