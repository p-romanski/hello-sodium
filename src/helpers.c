#include <stdio.h>
#include <sodium.h>

int print_bytes(char *label, const unsigned char *data, int length)
{
    int encoded_max_length = sodium_base64_ENCODED_LEN(length, sodium_base64_VARIANT_URLSAFE_NO_PADDING);
    fprintf(stderr, "DEBUG: print_bytes[%s]: data length=[%d], encoded_max_length=[%d]\n", label, length, encoded_max_length);
    char encoded_text[encoded_max_length];
    sodium_bin2base64(encoded_text, encoded_max_length, data, crypto_box_NONCEBYTES, sodium_base64_VARIANT_URLSAFE_NO_PADDING);

    fprintf(stdout, "%s: %s\n", label, data);
    fprintf(stdout, "%s: %s\n", label, encoded_text);
}