#include <sodium.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <argp.h>

// #define MESSAGE (const unsigned char *)"test"
// #define MESSAGE_LEN 4
// #define CIPHERTEXT_LEN (crypto_box_MACBYTES + MESSAGE_LEN)
#define MAX_MESSAGE_LENGTH 10000L

const char *argp_program_version = "Encrypt 1.0";
static char doc[] = "Encrypt -- libsodium authenticated encryption in action!";

/* A description of the arguments we accept. */
static char args_doc[] = "private_key public_key";

/* The options we understand. */
static struct argp_option options[] = {
    // {"verbose", 'v', 0, 0, "Produce verbose output"},
    // {"output", 'o', "FILE", 0, "Output to FILE instead of standard output"},
    {"pk", -1, "FILE", 0, "File containing public key of the receiver"},
    {"sk", -2, "FILE", 0, "File containing secret (private key) of the sender"},
    {"in", 'i', "FILE", 0, "Input file to encrypt"},
    {"out", 'o', "FILE", 0, "Output file to save encrypted message"},
    // {"nonce", 'n', "FILE", 0, "File containing nonce (if empty, random nonce will be generated)"},
    {0}};

/* Used by main to communicate with parse_opt. */
struct arguments
{
    char *pk_file;
    char *sk_file;
    char *input_file;
    char *output_file;

    char *args[2];
    int verbose;
};

/* Parse a single option. */
static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
    struct arguments *arguments = (struct arguments *)state->input;

    switch (key)
    {
    // case 'v':
    //     arguments->verbose = 1;
    //     break;
    case -1:
        arguments->pk_file = arg;
        break;
    case -2:
        arguments->sk_file = arg;
        break;
    case 'i':
        arguments->input_file = arg;
        break;
    case 'o':
        arguments->output_file = arg;
        break;
    case ARGP_KEY_ARG:
        switch (state->arg_num)
        {
            // case 0:
            //     arguments->sk_file = arg;
            // break;

            // case 1:
            //     arguments->pk_file = arg;
            // break;

        case 0:
            break;
        }
        fprintf(stdout, "ARGP_KEY_ARG #%d: %s\n", state->arg_num, arg);
        break;

    case ARGP_KEY_END:
        if (arguments->pk_file == 0)
        {
            fprintf(stderr, "Missing pk \n");
            argp_usage(state);
        }
        if (arguments->sk_file == 0)
        {
            fprintf(stderr, "Missing sk \n");
            argp_usage(state);
        }
        if (arguments->input_file == 0)
        {
            fprintf(stderr, "Missing input file \n");
            argp_usage(state);
        }
        if (arguments->output_file == 0)
        {
            fprintf(stderr, "Missing output file \n");
            argp_usage(state);
        }
        break;

        // case ARGP_KEY_ARG:
        //     if (state->arg_num >= 2)
        //         /* Too many arguments. */
        //         argp_usage(state);

        //     arguments->args[state->arg_num] = arg;
        //     break;

        // case ARGP_KEY_END:
        //     if (state->arg_num < 2)
        //         /* Not enough arguments. */
        //         argp_usage(state);
        //     break;

    default:
        return ARGP_ERR_UNKNOWN;
    }

    return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc};

size_t load_bytes(unsigned char *out, const char *path, size_t size)
{
    FILE *f = fopen(path, "rb");
    if (f == NULL)
        return -1;

    size_t bytes_read = fread(out, sizeof(unsigned char), size, f);
    fclose(f);

    return bytes_read;
}

long read_file(char **out, const char *path)
{
    FILE *f = fopen(path, "r");
    if (f == NULL)
        return -1;

    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);

    if (fsize > MAX_MESSAGE_LENGTH) 
    {
        fprintf(stderr, "ERR: Message too long: %ld > %ld", fsize, MAX_MESSAGE_LENGTH);
        return -1;
    }

    *out = malloc(fsize + 1);
    fread(*out, fsize, 1, f);
    fclose(f);

    fprintf(stderr, "Message: %s\n", *out);

    return fsize;
}

int save_bytes(char *path, const unsigned char *data, size_t size)
{
    FILE *file = fopen(path, "wb");
    fwrite(data, size, 1, file);
    fclose(file);
}

int main(int argc, char **argv)
{
    struct arguments arguments;
    arguments.pk_file = 0;

    argp_parse(&argp, argc, argv, 0, 0, &arguments);

    printf("Running encryption...\n");
    printf("  - Reading public key from %s\n", arguments.pk_file);
    printf("  - Reading secret (private key) from %s\n", arguments.sk_file);
    printf("  - Generating random nonce \n");

    if (sodium_init() < 0)
        return -1;

    unsigned char secret_key[crypto_box_SECRETKEYBYTES];
    size_t sk_load_result = load_bytes(secret_key, arguments.sk_file, crypto_box_SECRETKEYBYTES);
    if (sk_load_result != crypto_box_SECRETKEYBYTES)
    {
        fprintf(stderr, "Could not read secret key from '%s', result: %ld \n", arguments.sk_file, sk_load_result);
        return -1;
    }

    unsigned char public_key[crypto_box_PUBLICKEYBYTES];
    size_t pk_load_result = load_bytes(public_key, arguments.pk_file, crypto_box_PUBLICKEYBYTES);
    if (pk_load_result != crypto_box_PUBLICKEYBYTES)
    {
        fprintf(stderr, "Could not read public key from '%s', result: %ld \n", arguments.pk_file, pk_load_result);
        return -1;
    }

    unsigned char nonce[crypto_box_NONCEBYTES];
    randombytes_buf(nonce, sizeof nonce);

    char *message;
    long message_length = read_file(&message, arguments.input_file);
    if (message_length < 0) 
    {
        fprintf(stderr, "Could not read message from '%s', result: %ld \n", arguments.input_file, message_length);
        return -1;
    }

    unsigned long long ciphertext_length = crypto_box_MACBYTES + message_length;

    unsigned char ciphertext[ciphertext_length];
    int encryption_result = crypto_box_easy(ciphertext, message, message_length, nonce, public_key, secret_key);
    if (encryption_result != 0)
    {
        fprintf(stderr, "Encryption failed, result: %d \n", encryption_result);
        return -2;
    }

    save_bytes(arguments.output_file, ciphertext, ciphertext_length);

    return 0;
}
