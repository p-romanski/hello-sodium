#include <sodium.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int save_bytes_b64(char *path, const unsigned char *data, int length)
{
    const int variant = sodium_base64_VARIANT_ORIGINAL;

    FILE *file = fopen(path, "w");
    int encoded_max_length = sodium_base64_ENCODED_LEN(length, variant);
    char encoded_text[encoded_max_length];
    sodium_bin2base64(encoded_text, encoded_max_length, data, crypto_box_NONCEBYTES, variant);
    fprintf(file, "%s", encoded_text);
    fclose(file);
}

int save_bytes(char *path, const unsigned char *data, size_t size)
{
    const int variant = sodium_base64_VARIANT_ORIGINAL;

    FILE *file = fopen(path, "wb");
    fwrite(data, size, 1, file);
    fclose(file);
}

int main(int argc, char **argv)
{
    if (argc != 2)
    {
        printf("USAGE: %s <name>\n", argv[0]);
        return -2;
    }

    if (sodium_init() < 0)
    {
        return -1;
    }

    int name_length = strlen(argv[1]);
    char pub_name[name_length + 5];
    char priv_name[name_length + 6];
    strcat(pub_name, argv[1]);
    strcat(pub_name, ".pub");
    strcat(priv_name, argv[1]);
    strcat(priv_name, ".priv");

    unsigned char publickey[crypto_box_PUBLICKEYBYTES];
    unsigned char secretkey[crypto_box_SECRETKEYBYTES];
    crypto_box_keypair(publickey, secretkey);

    save_bytes(pub_name, publickey, crypto_box_PUBLICKEYBYTES);
    save_bytes(priv_name, secretkey, crypto_box_SECRETKEYBYTES);
}
